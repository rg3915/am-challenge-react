# am-challenge-react

Teste para AMcom baseado em DRF + ReactJS + Docker

Leia os requisitos do projeto em [desafio.md](desafio.md)

## Este projeto foi feito com:

* [Python 3.8.2](https://www.python.org/)
* [Django 2.2.15](https://www.djangoproject.com/)
* [Django Rest Framework 3.11.1](https://www.django-rest-framework.org/)
* [react 16.13.1](https://pt-br.reactjs.org/)
* [axios 0.20.0](https://github.com/axios/axios)
* [react-bootstrap 1.3.0](https://react-bootstrap.github.io/getting-started/introduction)
* [React Bootstrap Typeahead](https://www.npmjs.com/package/react-bootstrap-typeahead)
* [Cypress](https://www.cypress.io/)
* [sweetalert2](https://sweetalert2.github.io/)

## Como rodar o projeto?

```bash
git clone https://gitlab.com/rg3915/am-challenge-react.git
cd am-challenge-react
```

### Sem Docker faça os passos a seguir

* Crie um virtualenv com Python 3.
* Ative o virtualenv.
* Instale as dependências.
* Rode as migrações.
* Rode os testes.
* Crie um superusuário.

```bash
# Rodando o backend
python3 -m venv .venv
source .venv/bin/activate
pip install -U pip && pip install -r requirements.txt
python contrib/env_gen.py
echo -e "\nDOCKER=False" >> .env
python manage.py migrate
python manage.py test
python manage.py create_data
python manage.py createsuperuser --username='admin' --email=''
```

### Rodando o frontend

Numa outra aba do terminal faça

```bash
cd frontend
npm install
npm start
```


### Rodando com Docker

```bash
cp contrib/env_sample .env
docker-compose -f docker-compose.dev.yml up --build -d
```

Entre no container e crie um superuser:

```bash
docker container exec -it \
am-challenge_app_1 \
python manage.py createsuperuser \
--username="admin" --email=""
```

Criando alguns dados

```bash
docker container exec -it \
am-challenge_app_1 \
python manage.py create_data
```

## Passo a passo do front com ReactJS

```bash
create-react-app frontend
cd frontend
npm install react-router-dom
npm install axios
npm install react-bootstrap bootstrap
npm install --save react-bootstrap-typeahead
npm start

cd src
mkdir services components containers
touch services/api.js
touch components/{Items,Navbar}.jsx
touch containers/{Sale,CartItems}.jsx
```



## Tabelas

![mer.png](img/mer.png)

## Swagger

https://django-rest-swagger.readthedocs.io/en/latest/

url: `/doc/`

![swagger.png](img/swagger.png)

## Postman

Sem Docker a url base é `http://localhost:8000/`.

Com Docker é `http://0.0.0.0:8000/`.

http://localhost:8000/api/sale/sales/

`POST > JSON(application/json)`

```json
{
    "customer": 1,
    "seller": 1
}
```

`/api/sale/saleitems/`

```json
{
    "sale": 1,
    "product": 1,
    "quantity": 3,
    "price_sale": 1.5
}
```

> id dos produtos: 1 a 4.

## Relatórios

1. Para calcular a comissão total do vendedor

`/api/sale/sales/report_total_comission/`

e os parâmetros:

`start_date` = data no formato `yyyy-mm-dd`

`end_date` = data no formato `yyyy-mm-dd`

`seller_pk` = id do vendedor

Exemplo:

`/api/sale/sales/report_total_comission/?start_date=2020-08-23&end_date=2020-08-30&seller_pk=1`


2. Quais produtos e serviços um determinado cliente comprou num intervalo de tempo?

`/api/sale/sales/report_sales_product/`

e os parâmetros:

`start_date` = data no formato `yyyy-mm-dd`

`end_date` = data no formato `yyyy-mm-dd`

`customer_pk` = id do cliente


Exemplo:

`/api/sale/sales/report_sales_product/?start_date=2020-08-23&end_date=2020-08-30&customer_pk=1`


3. Produtos mais vendidos num intervalo de tempo.

`/api/sale/sales/report_top_products/`

e os parâmetros:

`start_date` = data no formato `yyyy-mm-dd`

`end_date` = data no formato `yyyy-mm-dd`

Exemplo:

`/api/sale/sales/report_top_products/?start_date=2020-08-23&end_date=2020-08-30`

## Endpoints

Parâmetros adicionais dos endpoints.

```
/api/sale/sales/report_sales_product/?start_date=2020-08-25&end_date=2020-08-30&customer_pk=1
/api/sale/sales/report_top_products/?start_date=2020-08-25&end_date=2020-08-30
/api/sale/sales/report_total_comission/?start_date=2020-08-25&end_date=2020-08-30&seller_pk=1
```



## Cypress

Na pasta `frontend` digite

`$ ./node_modules/.bin/cypress open`


## Links

https://www.django-rest-framework.org/

http://www.cdrf.co/

https://pt-br.reactjs.org/

https://github.com/trevoreyre/autocomplete

https://www.cypress.io/

https://django-rest-swagger.readthedocs.io/en/latest/

https://www.django-rest-framework.org/community/3.10-announcement/#continuing-to-use-coreapi

https://github.com/ankurk91/vue-toast-notification

https://sweetalert2.github.io/
