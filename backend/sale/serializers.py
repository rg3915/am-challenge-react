from rest_framework import serializers

from backend.crm.serializers import CustomerSerializer, SellerSerializer
from backend.product.serializers import ProductSerializer

from .models import Sale, SaleItems


class SaleSerializer(serializers.ModelSerializer):
    # customer = CustomerSerializer()
    # seller = SellerSerializer()
    get_total_comission = serializers.ReadOnlyField()

    class Meta:
        model = Sale
        exclude = ('id', 'created', 'modified')


class SaleItemsSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = SaleItems
        fields = ('sale', 'product', 'quantity', 'price_sale', 'comission')
