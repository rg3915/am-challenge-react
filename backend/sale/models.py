from datetime import datetime, time
from decimal import ROUND_UP, Decimal

from django.db import models
from django.utils import timezone
from django.utils.formats import number_format

from backend.core.models import TimeStampedModel
from backend.crm.models import Customer, Seller
from backend.product.models import Product


class Sale(TimeStampedModel):
    customer = models.ForeignKey(
        Customer,
        related_name='customer_sale',
        verbose_name='cliente',
        on_delete=models.PROTECT,
        blank=True
    )
    seller = models.ForeignKey(
        Seller,
        related_name='seller_sale',
        verbose_name='vendedor',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    class Meta:
        ordering = ('-created',)
        verbose_name = 'venda'
        verbose_name_plural = 'vendas'

    def __str__(self):
        return "%03d" % self.id + "/%s" % self.created.strftime('%y')
    code = property(__str__)

    def get_total_price(self):
        qs = self.sales.filter(sale=self.pk)\
            .values_list('price_sale', 'quantity') or 0
        t = 0 if isinstance(qs, int) else sum(map(lambda q: q[0] * q[1], qs))
        return Decimal(t)

    def get_total_comission(self):
        qs = self.sales.filter(sale=self.pk)\
            .values_list('comission', flat=True) or 0
        t = 0 if isinstance(qs, int) else sum(qs)
        return Decimal(t)


class SaleItems(models.Model):
    sale = models.ForeignKey(
        Sale,
        related_name='sales',
        on_delete=models.CASCADE,
    )
    product = models.ForeignKey(
        Product,
        related_name='product_det',
        verbose_name='produto',
        on_delete=models.CASCADE,
    )
    quantity = models.DecimalField(
        'quantidade',
        max_digits=6,
        decimal_places=2,
        default=0.0
    )
    price_sale = models.DecimalField(
        'preço de venda',
        max_digits=6,
        decimal_places=2,
        default=0.0
    )
    comission = models.DecimalField(
        'comissão',
        max_digits=6,
        decimal_places=2,
        default=0.0,
        editable=False,
        help_text='Comissão final em reais.'
    )

    def __str__(self):
        return str(self.sale)

    def save(self, *args, **kwargs):
        self.comission = self.get_comission()
        super(SaleItems, self).save(*args, **kwargs)

    def get_subtotal(self):
        return self.price_sale * (self.quantity or 0)

    def is_time_less_midday(self):
        '''
        Se hora for menor ou igual a 12:00:00 retorna True,
        caso contrário retorna False.
        '''
        return bool(self.sale.created.time() <= time(12, 0, 0))

    def get_comission(self):
        '''
        Cada produto ou serviço (item de venda) quando vendido, pode gerar
        uma comissão entre 0 e 10%,
        se a venda for entre 00:00 e 12:00 a comissão de cada item de venda
        não pode ser maior que 5%,
        se a venda for entre 12:00:01 e 23:59:59 a comissão de cada item de
        venda deve ser no mínimo 4%.
        '''
        comission = self.product.comission
        if self.is_time_less_midday():
            if comission > 0.05:
                comission = 0.05
        else:
            if comission < 0.04:
                comission = 0.04
        self.comission = Decimal(self.price_sale) * \
            Decimal(self.quantity) * Decimal(comission)
        return Decimal(str(self.comission)).quantize(
            Decimal('0.01'),
            rounding=ROUND_UP
        )

    subtotal = property(get_subtotal)
