from datetime import timedelta

from dateutil.parser import parse
from django.core.exceptions import ObjectDoesNotExist, SuspiciousOperation
from django.db import IntegrityError
from django.http import Http404
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.views import Response

from backend.crm.models import Customer, Seller
from backend.product.models import Product

from .mixins import ReportMixin
from .models import Sale, SaleItems
from .serializers import SaleItemsSerializer, SaleSerializer


class SaleViewSet(ReportMixin, viewsets.ModelViewSet):
    queryset = Sale.objects.all().select_related('customer')
    serializer_class = SaleSerializer

    def get_queryset(self):
        # Aplica um filtro entre duas datas.
        queryset = super(SaleViewSet, self).get_queryset()
        start_date = self.request.query_params.get('start_date')
        end_date = self.request.query_params.get('end_date')

        if start_date and end_date:
            end_date = parse(end_date) + timedelta(1)
            queryset = queryset.filter(
                created__range=[start_date, end_date]
            )
        return queryset


class SaleItemsViewSet(viewsets.ModelViewSet):
    queryset = SaleItems.objects.all()
    serializer_class = SaleItemsSerializer

    def create(self, request, *args, **kwargs):
        try:
            Product.objects.get(pk=request.data.get('product'))
        except ObjectDoesNotExist:
            raise Http404
        try:
            return super(SaleItemsViewSet, self).create(request, *args, **kwargs)
        except IntegrityError:
            if int(request.data.get('quantity')) < 0:
                response = Response(
                    {'message': 'Quantidade não pode ser menor que zero.'},
                    status=status.HTTP_400_BAD_REQUEST
                )
                return response
            raise SuspiciousOperation

    @action(methods=['post'], detail=False)
    def add(self, request, *args, **kwargs):
        customer_pk = request.data.get('customer')
        seller_pk = request.data.get('seller')
        items = request.data.get('items')

        # Retorna o cliente
        customer = Customer.objects.get(pk=customer_pk)

        # Retorna o vendedor
        seller = Seller.objects.get(pk=seller_pk)

        # Cria a venda
        sale = Sale.objects.create(customer=customer, seller=seller)

        # Adiciona os itens da venda.
        for item in items:
            # Retorna o produto
            product_pk = item['product']
            product = Product.objects.get(pk=product_pk)
            # Insere os produtos como itens da venda.
            sale_items = SaleItems.objects.create(
                sale=sale,
                product=product,
                quantity=item['quantity'],
                price_sale=item['price_sale'],
            )
            data = {
                'pk': sale_items.sale.pk,
                'status': status.HTTP_201_CREATED
            }
        return Response(data)
