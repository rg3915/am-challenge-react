from datetime import datetime
from decimal import Decimal

import pytz
from django.test import TestCase
from faker import Faker

from backend.crm.models import Customer, Seller
from backend.product.models import Product
from backend.sale.models import Sale, SaleItems


class TestSale(TestCase):

    def setUp(self) -> None:
        self.faker = Faker()
        self.customer = Customer.objects.create(name=self.faker.name())
        self.seller = Seller.objects.create(name=self.faker.name())

        self.sale = Sale.objects.create(
            customer=self.customer,
            seller=self.seller
        )
        # Altera a data de criação para de manhã.
        self.sale.created = datetime(
            2020, 8, 23, 11, 0, 0, 0, tzinfo=pytz.UTC)
        self.sale.save()

        self.product1 = Product.objects.create(
            title='Caneta',
            price=1.5,
            comission=0.01
        )
        self.product2 = Product.objects.create(
            title='Borracha',
            price=0.5,
            comission=0.05
        )
        self.sale_items1 = SaleItems.objects.create(
            sale=self.sale,
            product=self.product1,
            quantity=3,
            price_sale=self.product1.price,
        )
        self.sale_items2 = SaleItems.objects.create(
            sale=self.sale,
            product=self.product2,
            quantity=5,
            price_sale=self.product2.price,
        )

    def test_sale_deve_retornar_atributos(self):
        fields = (
            'customer',
            'seller',
            'created',
            'modified',
        )

        for field in fields:
            with self.subTest():
                self.assertTrue(hasattr(Sale, field))

    def test_sale_str_deve_retornar_nome(self):
        esperado = '001/20'
        resultado = str(self.sale)
        self.assertEqual(esperado, resultado)

    def test_sale_get_total_price(self):
        esperado = Decimal('7.00')
        resultado = self.sale.get_total_price()
        self.assertEqual(esperado, resultado)

    def test_sale_get_total_comission(self):
        esperado = Decimal('0.18')
        resultado = self.sale.get_total_comission()
        self.assertEqual(esperado, resultado)


class TestSaleItems(TestCase):

    def setUp(self) -> None:
        self.faker = Faker()
        self.customer = Customer.objects.create(name=self.faker.name())
        self.seller = Seller.objects.create(name=self.faker.name())

        self.product1 = Product.objects.create(
            title='Caneta',
            price=1.5,
            comission=0.01
        )
        self.product2 = Product.objects.create(
            title='Borracha',
            price=4.5,
            comission=0.09
        )

        self.sale1 = Sale.objects.create(
            customer=self.customer,
            seller=self.seller,
        )
        # Altera a data de criação para de manhã.
        self.sale1.created = datetime(
            2020, 8, 23, 11, 0, 0, 0, tzinfo=pytz.UTC)
        self.sale1.save()

        self.sale_items1 = SaleItems.objects.create(
            sale=self.sale1,
            product=self.product1,
            quantity=3,
            price_sale=self.product1.price
        )
        self.sale_items2 = SaleItems.objects.create(
            sale=self.sale1,
            product=self.product2,
            quantity=3,
            price_sale=self.product2.price
        )

        self.sale2 = Sale.objects.create(
            customer=self.customer,
            seller=self.seller,
        )
        # Altera a data de criação para de tarde.
        self.sale2.created = datetime(
            2020, 8, 23, 13, 0, 0, 0, tzinfo=pytz.UTC)
        self.sale2.save()

        self.sale_items3 = SaleItems.objects.create(
            sale=self.sale2,
            product=self.product1,
            quantity=3,
            price_sale=self.product1.price
        )
        self.sale_items4 = SaleItems.objects.create(
            sale=self.sale2,
            product=self.product2,
            quantity=3,
            price_sale=self.product2.price
        )

    def test_sale_items_deve_retornar_atributos(self):
        fields = (
            'sale',
            'product',
            'quantity',
            'price_sale',
        )

        for field in fields:
            with self.subTest():
                self.assertTrue(hasattr(SaleItems, field))

    def test_sale_items_str_deve_retornar_nome(self):
        esperado = '001/20'
        resultado = str(self.sale_items1)
        self.assertEqual(esperado, resultado)

    def test_sale_items_get_subtotal(self):
        esperado = Decimal('4.50')
        resultado = self.sale_items1.get_subtotal()
        self.assertEqual(esperado, resultado)

    def test_sale_items1_get_comission(self):
        esperado = Decimal('0.05')
        resultado = self.sale_items1.get_comission()
        self.assertEqual(esperado, resultado)

    def test_sale_items2_get_comission(self):
        esperado = Decimal('0.68')
        resultado = self.sale_items2.get_comission()
        self.assertEqual(esperado, resultado)

    def test_sale_items3_get_comission(self):
        esperado = Decimal('0.19')
        resultado = self.sale_items3.get_comission()
        self.assertEqual(esperado, resultado)

    def test_sale_items4_get_comission(self):
        esperado = Decimal('1.22')
        resultado = self.sale_items4.get_comission()
        self.assertEqual(esperado, resultado)
