from django.test import TestCase
from faker import Faker

from backend.crm.models import Customer, Seller
from backend.product.models import Product
from backend.sale.models import Sale, SaleItems


class TestUrl(TestCase):

    def setUp(self) -> None:
        self.faker = Faker()
        self.customer = Customer.objects.create(name=self.faker.name())
        self.seller = Seller.objects.create(name=self.faker.name())

        self.sale = Sale.objects.create(
            customer=self.customer,
            seller=self.seller
        )
        self.product1 = Product.objects.create(
            title='Caneta',
            price=1.5,
            comission=0.01
        )
        self.product2 = Product.objects.create(
            title='Borracha',
            price=0.5,
            comission=0.05
        )
        self.sale_items = SaleItems.objects.create(
            sale=self.sale,
            product=self.product1,
            quantity=3,
            price_sale=self.product1.price,
        )

    def test_url_sales(self):
        url = '/api/sale/sales/'

        self.r = self.client.get(url)
        self.assertEqual(200, self.r.status_code)

    def test_url_sale_detail(self):
        url = '/api/sale/sales/1/'

        self.r = self.client.get(url)
        self.assertEqual(200, self.r.status_code)

    def test_url_sale_items(self):
        url = '/api/sale/saleitems/'

        self.r = self.client.get(url)
        self.assertEqual(200, self.r.status_code)

    def test_url_sale_detail(self):
        url = '/api/sale/saleitems/1/'

        self.r = self.client.get(url)
        self.assertEqual(200, self.r.status_code)
