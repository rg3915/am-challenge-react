import json
from datetime import datetime

import pytz
from django.test import TestCase
from rest_framework import status

from backend.crm.models import Customer, Seller
from backend.product.models import Product
from backend.sale.models import Sale, SaleItems
from backend.sale.serializers import SaleItemsSerializer, SaleSerializer


class TestReportTotalComission(TestCase):
    '''
    Testa o retorno da comissão de um vendedor específico num intervalo de tempo.
    '''

    def setUp(self) -> None:
        self.customer = Customer.objects.create(name='Regis')
        self.seller = Seller.objects.create(name='Regis')

        self.sale = Sale.objects.create(
            customer=self.customer,
            seller=self.seller
        )
        # Altera a data de criação para de manhã.
        self.sale.created = datetime(
            2020, 8, 30, 11, 0, 0, 0, tzinfo=pytz.UTC)
        self.sale.save()

        self.product1 = Product.objects.create(
            title='Caneta',
            price=1.5,
            comission=0.01
        )
        self.product2 = Product.objects.create(
            title='Borracha',
            price=0.5,
            comission=0.05
        )
        self.sale_items1 = SaleItems.objects.create(
            sale=self.sale,
            product=self.product1,
            quantity=3,
            price_sale=self.product1.price,
        )

    def test_comissao_vendedor(self):
        endpoint = '/api/sale/sales/report_total_comission/'
        query_parameters = '?start_date=2020-08-30&end_date=2020-08-30&seller_pk=1'
        url = f'{endpoint}{query_parameters}'
        response = self.client.get(url, content_type='application/json')

        resultado = json.loads(response.content)
        esperado = {
            'seller': {
                'id': 1,
                'name': 'Regis'
            },
            'start_date': '2020-08-30',
            'end_date': '2020-08-31T00:00:00',
            'total_comission': 0.05,
        }

        self.assertEqual(esperado, resultado)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
