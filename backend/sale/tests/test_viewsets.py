import json
from datetime import datetime

import pytz
from django.test import TestCase
from faker import Faker
from rest_framework import status

from backend.crm.models import Customer, Seller
from backend.product.models import Product
from backend.sale.models import Sale, SaleItems
from backend.sale.serializers import SaleItemsSerializer, SaleSerializer


class TestViewsets(TestCase):

    def setUp(self) -> None:
        self.faker = Faker()
        self.customer = Customer.objects.create(name=self.faker.name())
        self.seller = Seller.objects.create(name=self.faker.name())

        self.sale = Sale.objects.create(
            customer=self.customer,
            seller=self.seller
        )
        # Altera a data de criação para de manhã.
        self.sale.created = datetime(
            2020, 8, 30, 11, 0, 0, 0, tzinfo=pytz.UTC)
        self.sale.save()

        self.product1 = Product.objects.create(
            title='Caneta',
            price=1.5,
            comission=0.01
        )
        self.product2 = Product.objects.create(
            title='Borracha',
            price=0.5,
            comission=0.05
        )
        self.sale_items = SaleItems.objects.create(
            sale=self.sale,
            product=self.product1,
            quantity=3,
            price_sale=self.product1.price,
        )
        self.sale_serializer = SaleSerializer(instance=self.sale)
        self.sale_items_serializer = SaleItemsSerializer(
            instance=self.sale_items)

    def test_sale_viewsets(self):
        response = self.client.get(
            '/api/sale/sales/',
            content_type='application/json'
        )
        resultado = json.loads(response.content)
        esperado = [
            {
                'customer': 1,
                'seller': 1,
                'get_total_comission': 0.05,
            }
        ]

        self.assertEqual(esperado, resultado)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_sale_viewsets_detail(self):
        response = self.client.get(
            '/api/sale/sales/1/',
            content_type='application/json'
        )
        resultado = json.loads(response.content)
        esperado = {
            'customer': 1,
            'seller': 1,
            'get_total_comission': 0.05,
        }

        self.assertEqual(esperado, resultado)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_sale_items_viewsets(self):
        response = self.client.get(
            '/api/sale/saleitems/',
            content_type='application/json'
        )
        resultado = json.loads(response.content)
        esperado = [
            {
                'sale': 1,
                'product': {
                    'comission': '0.01',
                    'id': 1,
                    'price': '1.50',
                    'title': 'Caneta'
                },
                'price_sale': '1.50',
                'quantity': '3.00',
                'comission': '0.05',
            }
        ]

        self.assertEqual(esperado, resultado)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_sale_items_viewsets_detail(self):
        response = self.client.get(
            '/api/sale/saleitems/1/',
            content_type='application/json'
        )
        resultado = json.loads(response.content)
        esperado = {
            'sale': 1,
            'product': {
                'comission': '0.01',
                'id': 1,
                'price': '1.50',
                'title': 'Caneta'
            },
            'price_sale': '1.50',
            'quantity': '3.00',
            'comission': '0.05',
        }

        self.assertEqual(esperado, resultado)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
