import json
from datetime import datetime

import pytz
from django.test import TestCase
from rest_framework import status

from backend.crm.models import Customer, Seller
from backend.product.models import Product
from backend.sale.models import Sale, SaleItems
from backend.sale.serializers import SaleItemsSerializer, SaleSerializer


class TestCreateSale(TestCase):
    '''
    Testa a finalização de uma venda.
    '''

    def setUp(self) -> None:
        self.endpoint = '/api/sale/saleitems/add/'
        self.payload = {
            'customer': '1',
            'seller': '1',
            'items': [
                {
                    'product': '1',
                    'quantity': '1',
                    'price_sale': '1.50'
                },
                {
                    'product': '2',
                    'quantity': '1',
                    'price_sale': '2.50'
                }
            ]
        }
        self.customer = Customer.objects.create(name='Regis')
        self.seller = Seller.objects.create(name='Regis')

        self.product1 = Product.objects.create(
            title='Caneta',
            price=1.5,
            comission=0.01
        )
        self.product2 = Product.objects.create(
            title='Borracha',
            price=0.5,
            comission=0.05
        )

    def test_venda(self):
        response = self.client.post(
            self.endpoint,
            data=self.payload,
            content_type='application/json'
        )

        resultado = json.loads(response.content)
        esperado = {'pk': 1, 'status': 201}

        self.assertEqual(esperado, resultado)

    def test_venda_status(self):
        response = self.client.post(
            self.endpoint,
            data=self.payload,
            content_type='application/json'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_venda_exists(self):
        response = self.client.post(
            self.endpoint,
            data=self.payload,
            content_type='application/json'
        )

        resultado = json.loads(response.content)

        self.assertTrue(Sale.objects.filter(pk=resultado.get('pk')).exists())
