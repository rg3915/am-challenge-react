from datetime import timedelta

from dateutil.parser import parse
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Sum
from rest_framework.decorators import action
from rest_framework.views import Response

from backend.crm.models import Customer, Seller
from backend.crm.serializers import CustomerSerializer, SellerSerializer
from backend.product.models import Product
from backend.product.serializers import ProductSerializer

from .models import Sale, SaleItems
from .serializers import SaleItemsSerializer


class ReportMixin:

    @action(detail=False)
    def report_total_comission(self, request, **kwargs):
        '''
        Dado um intervalo de tempo, e um vendedor, quanto de comissão
        um vendedor tem direito?
        '''
        seller_pk = self.request.query_params.get('seller_pk')
        start_date = self.request.query_params.get('start_date')
        end_date = self.request.query_params.get('end_date')
        data = {}

        try:
            Seller.objects.get(pk=seller_pk)
        except ObjectDoesNotExist:
            # raise Http404
            return Response('Favor informar o id do vendedor.')

        if not start_date:
            return Response('Favor informar a data inicial.')

        if not end_date:
            return Response('Favor informar a data final.')

        if start_date and end_date:
            # Converte string em data e adiciona um dia.
            end_date = parse(end_date) + timedelta(1)
            # Retorna o vendedor.
            seller = Seller.objects.get(pk=seller_pk)
            # Filtra pela data e pelo vendedor.
            queryset = Sale.objects.filter(
                created__range=[start_date, end_date],
                seller=seller
            )
            # Calcula o total da comissão do vendedor.
            total_comission = sum([
                item.get_total_comission() for item in queryset
            ])
            data = {
                'seller': SellerSerializer(seller).data,
                'start_date': start_date,
                'end_date': end_date,
                'total_comission': total_comission
            }
        return Response(data)

    @action(detail=False)
    def report_sales_product(self, request, **kwargs):
        '''
        Quais produtos um determinado cliente comprou num intervalo de tempo?
        '''
        customer_pk = self.request.query_params.get('customer_pk')
        start_date = self.request.query_params.get('start_date')
        end_date = self.request.query_params.get('end_date')
        data = {}

        try:
            Customer.objects.get(pk=customer_pk)
        except ObjectDoesNotExist:
            # raise Http404
            return Response('Favor informar o id do cliente.')

        if not start_date:
            return Response('Favor informar a data inicial.')

        if not end_date:
            return Response('Favor informar a data final.')

        if start_date and end_date:
            # Converte string em data e adiciona um dia.
            end_date = parse(end_date) + timedelta(1)
            # Retorna o vendedor.
            customer = Customer.objects.get(pk=customer_pk)
            # Filtra pela data e pelo cliente.
            sale_items = SaleItems.objects.filter(
                sale__created__range=[start_date, end_date],
                sale__customer=customer
            ).values_list('product').distinct()
            # Retorna os produtos que o cliente comprou.
            products = Product.objects.filter(pk__in=sale_items)
            data = {
                'customer': CustomerSerializer(customer).data,
                'start_date': start_date,
                'end_date': end_date,
                'products': ProductSerializer(products, many=True).data
            }
        return Response(data)

    @action(detail=False)
    def report_sales_product_detail(self, request, **kwargs):
        '''
        Retorna a compra do cliente. Requer sale_id.
        '''
        sale_pk = self.request.query_params.get('sale_pk')
        data = {}

        try:
            sale = Sale.objects.get(pk=sale_pk)
        except ObjectDoesNotExist:
            return Response('Favor informar o id da venda.')

        # Retorna os produtos que o cliente comprou.
        sale_items = SaleItems.objects.filter(sale__pk=sale_pk)
        data = {
            'items': SaleItemsSerializer(sale_items, many=True).data,
            'total': sale.get_total_price(),
            'code': sale.__str__()
        }
        return Response(data)

    @action(detail=False)
    def report_top_products(self, request, **kwargs):
        '''
        Retorna os produtos mais vendidos num intervalo de tempo,
        em ordem decrescente.
        '''
        start_date = self.request.query_params.get('start_date')
        end_date = self.request.query_params.get('end_date')
        data = {}

        if not start_date:
            return Response('Favor informar a data inicial.')

        if not end_date:
            return Response('Favor informar a data final.')

        if start_date and end_date:
            # Converte string em data e adiciona um dia.
            end_date = parse(end_date) + timedelta(1)
            # Filtra pela data e já conta os produtos mais vendidos.
            products = SaleItems.objects.filter(
                sale__created__range=[start_date, end_date])\
                .values('product')\
                .annotate(count=Sum('quantity'))\
                .values('product__title', 'count')\
                .order_by('-count')
            data = {
                'start_date': start_date,
                'end_date': end_date,
                'products': products
            }
        return Response(data)
