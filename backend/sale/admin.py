from django.contrib import admin

from .models import Sale, SaleItems


class SaleItemsInline(admin.TabularInline):
    list_display = ('product', 'quantity', 'price_sale')
    readonly_fields = ('get_subtotal', 'get_comission')
    model = SaleItems
    extra = 0


@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    list_display = (
        '__str__',
        'customer',
        'seller',
        'created',
        'get_total_price',
        'get_total_comission'
    )
    readonly_fields = ('get_total_price', 'get_total_comission')
    date_hierarchy = 'created'
    list_filter = ('customer',)
    inlines = (SaleItemsInline,)
