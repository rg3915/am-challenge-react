from django.contrib import admin
from django.urls import include, path
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='API')

urlpatterns = [
    path('api/crm/', include('backend.crm.urls', namespace='crm')),
    path('api/product/', include('backend.product.urls', namespace='product')),
    path('api/sale/', include('backend.sale.urls', namespace='sale')),
    path('admin/', admin.site.urls),
    path('doc/', schema_view)
]
