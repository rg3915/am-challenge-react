from django.test import TestCase

from backend.product.models import Product


class UrlTest(TestCase):

    def setUp(self) -> None:
        self.product = Product.objects.create(title='Caneta', price=1.5)

    def test_url_products(self):
        url = '/api/product/products/'

        self.r = self.client.get(url)
        self.assertEqual(200, self.r.status_code)

    def test_url_product_detail(self):
        url = '/api/product/products/1/'

        self.r = self.client.get(url)
        self.assertEqual(200, self.r.status_code)
