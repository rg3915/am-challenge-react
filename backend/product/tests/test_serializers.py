from django.test import TestCase

from backend.product.models import Product
from backend.product.serializers import ProductSerializer


class UrlTest(TestCase):

    def setUp(self) -> None:
        self.product = Product.objects.create(title='caneta', price=1.5)
        self.serializer = ProductSerializer(instance=self.product)

    def test_product_contem_campos_esperados(self):
        data = self.serializer.data
        esperado = set(['id', 'title', 'price', 'comission'])
        resultado = set(data.keys())

        self.assertEqual(esperado, resultado)
