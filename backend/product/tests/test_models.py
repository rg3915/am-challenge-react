from django.test import TestCase

from backend.product.models import Product


class TestProduct(TestCase):

    def setUp(self) -> None:
        self.product = Product.objects.create(
            title='Caneta',
            price=1.5,
            comission=0.1,
        )

    def test_product_deve_retornar_atributos(self):
        fields = (
            'title',
            'price',
            'comission',
        )

        for field in fields:
            with self.subTest():
                self.assertTrue(hasattr(Product, field))

    def test_produtct_str_deve_retornar_nome(self):
        esperado = 'Caneta'
        resultado = str(self.product)
        self.assertEqual(esperado, resultado)
