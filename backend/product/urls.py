from django.urls import include, path
from rest_framework import routers

from backend.product import views as v

app_name = 'product'

router = routers.DefaultRouter()
router.register(r'products', v.ProductViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
