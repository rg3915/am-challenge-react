import React from 'react'
import './App.css';

import MyNavbar from './components/Navbar'
import Home from './containers/Home'
import Sale from './containers/Sale'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        <MyNavbar/>
      </div>
      <Switch>
        <Route path="/sale">
          <Sale />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
