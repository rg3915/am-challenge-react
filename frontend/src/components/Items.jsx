import React, { useState, useEffect } from 'react'
import { listProducts } from '../services/api.js'
import Table from 'react-bootstrap/Table';

const formatPrice = (value) => {
  return value.toLocaleString('pt-BR', {minimumFractionDigits: 2})
}

export default ({data}) => {
  return (
    <Table bordered hover>
      <thead>
        <tr>
          <th>Produto/Serviço</th>
          <th>Quantidade</th>
          <th>Preço unitário</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        {data.map((item, idx) =>
          <tr key={idx}>
            <td>{ item.product.title }</td>
            <td>{ item.quantity }</td>
            <td>
              <span className="float-left">R$</span> <span className="float-right">{ formatPrice(item.product.price) }</span>
            </td>
            <td>
              <span className="float-left">R$</span> <span className="float-right">{ formatPrice(item.quantity * item.product.price) }</span>
            </td>
          </tr>
        )}
      </tbody>
    </Table>
  )
}