import React, { useState, useEffect } from 'react'
import logo from '../logo.svg';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

export default MyNavbar => {
  return (
    <Navbar bg="dark" variant="dark" expand="lg">
      <Navbar.Brand href="">
        <img src={logo} className="App-logo" width="50px" alt="logo" />
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/">Home</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}
