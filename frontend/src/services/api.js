import axios from 'axios';

const baseUrl = 'http://localhost:8000/api'

export const listProducts = () =>
  axios.get(`${baseUrl}/product/products/`)
    .then(({ data }) => data)

export const listCustomers = () =>
  axios.get(`${baseUrl}/crm/customers/`)
    .then(({ data }) => data)

export const listSellers = () =>
  axios.get(`${baseUrl}/crm/sellers/`)
    .then(({ data }) => data)

