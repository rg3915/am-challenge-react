import React from 'react'
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';

export default () => {

  return (
    <div className="text-center">
      <p>Este é um projeto feito com <a href="https://www.django-rest-framework.org/" target="_blank">Django Rest Framework</a> e <a href="https://pt-br.reactjs.org/" target="_blank">ReactJS</a>.</p>
      <p><a href="https://gitlab.com/rg3915/am-challenge-react/" target="_blank">Veja no GitLab</a></p>
      <Link to="/sale">
        <Button variant="primary">Fazer uma compra</Button>
      </Link>
    </div>
  )
}