import React, { useState, useEffect, useRef } from 'react'
import Table from 'react-bootstrap/Table';
import { listProducts, listCustomers, listSellers } from '../services/api.js'
import Items from '../components/Items'
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

import axios from 'axios';

import { Typeahead } from 'react-bootstrap-typeahead'; // ES2015

const useForm = submitCallback => {
  const [state, setState] = useState({});

  const handleSubmit = e => {
    e.preventDefault()
    submitCallback()
  }

  const handleChange = e => {
    e.persist()
    setState(state => ({ ...state, [e.target.name]: e.target.value }))
  }

  const handleChangeAutocomplete = selected => {
    setState(state => ({ ...state, product: selected[0] }))
  }

  return [state, handleSubmit, handleChange, handleChangeAutocomplete, setState]
}

export default Sales => {
  const formSubmit = () => {
    let url = 'http://localhost:8000/api/sale/saleitems/add/'
    let items = cartItems
    let data = {}
    data.items = items.map(item => {
      const {product, quantity} = item
      return {product: product.id, price_sale: product.price, quantity: quantity}
    })
    data.customer = values.customer
    data.seller = values.seller
    console.log(data);
    axios.post(url, data)
  }

  const ref = useRef()
  const [products, setProducts] = useState([])
  const [cartItems, setCartItems] = useState([])
  const [customers, setCustomers] = useState([])
  const [sellers, setSellers] = useState([])
  const [disableAdd, setDisableAdd] = useState(true)
  const [values, handleSubmit, handleChange, handleChangeAutocomplete, setValues] = useForm(formSubmit)

  useEffect(() => {
    listProducts()
      .then(data => {
        setProducts(data)
      })
    listCustomers()
      .then(data => {
        setCustomers(data)
      })
    listSellers()
      .then(data => {
        setSellers(data)
      })
  }, [])

  const addProduct = (value) => {
    if (!value.product) {
      return
    }
    setCartItems([...cartItems, value])
    setValues({...values, product: null})
    ref.current.clear()
  }

  return (
    <Container>
      <Form onSubmit={handleSubmit}>
        <Row>
          <Col>
            <Row>
              <Col>
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Produto</Form.Label>
                  <Typeahead
                    id="product"
                    name="product"
                    labelKey="title"
                    onChange={handleChangeAutocomplete}
                    options={products}
                    ref={ref}
                  />
                  <Form.Text className="text-muted">
                    Buscar pelo código de barras (id) ou descrição
                  </Form.Text>
                </Form.Group>
              </Col>
              <Col md="2">
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Quantidade</Form.Label>
                  <Form.Control type="number" name="quantity" onChange={handleChange} min="0" step="0.01" placeholder="0" />
                </Form.Group>
              </Col>
              <Col md="auto">
                <Form.Label className="rta">.</Form.Label> <br/>
                <Button variant="secondary" disabled={disableAdd} onClick={() => addProduct(values)}>Adicionar</Button>
              </Col>
            </Row>
            <Items data={cartItems}/>
          </Col>
          <Col md="auto">
            <Form.Group controlId="exampleForm.SelectCustom">
              <Form.Label>Escolha um vendedor</Form.Label>
              <Form.Control name="customer" onChange={handleChange} as="select" custom>
                <option value="">Selecione</option>
                {customers.map(item =>
                  <option key={item.id} value={item.id}>{ item.name }</option>
                )}
              </Form.Control>
            </Form.Group>
            <Form.Group controlId="exampleForm.SelectCustom">
              <Form.Label>Escolha um cliente</Form.Label>
              <Form.Control name="seller" onChange={handleChange} as="select" custom>
                <option value="">Selecione</option>
                {sellers.map(item =>
                  <option key={item.id} value={item.id}>{ item.name }</option>
                )}
              </Form.Control>
            </Form.Group>
            <Button variant="light">Cancelar</Button>
            <Button type="submit" variant="success" className="float-right">Finalizar</Button>
          </Col>
        </Row>
      </Form>
    </Container>
  )
}